package com.na.mylauncher.models;

import android.graphics.drawable.Drawable;

public class AppsInfo {
    public CharSequence label;
    public CharSequence packageName;
    public Drawable icon;
}
