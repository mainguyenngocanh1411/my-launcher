package com.na.mylauncher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.na.mylauncher.adapters.RAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Run apps drawer
        Intent intent = new Intent(this, AppsDrawer.class);
        startActivity(intent);


    }

    @Override
    protected void onResume() {
        System.out.println("On resume");
        super.onResume();

        //Run apps drawer
        Intent intent = new Intent(this, AppsDrawer.class);
        startActivity(intent);
    }
}
