package com.na.mylauncher.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.na.mylauncher.R;
import com.na.mylauncher.models.AppsInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class RAdapter extends RecyclerView.Adapter<RAdapter.ViewHolder> implements ItemTouchHelperAdapter {
    private List<AppsInfo> appsList;
    String currentPosition;

    public RAdapter() {
        appsList = new ArrayList<>();



    }


    @NonNull
    @Override
    public RAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        System.out.println("Create view holder");
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View view = inflater.inflate(R.layout.item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RAdapter.ViewHolder viewHolder, int i) {
        System.out.println("Binding");
        String appLabel = appsList.get(i).label.toString();
        String packageName = appsList.get(i).packageName.toString();
        Drawable appIcon = appsList.get(i).icon;

        TextView textView = viewHolder.appName;
        textView.setText(appLabel);
        TextView appPackage = viewHolder.appPacakge;
        appPackage.setText(packageName);

        ImageView imageView = viewHolder.appIcon;
        imageView.setImageDrawable(appIcon);



    }

    @Override
    public int getItemCount() {
        return appsList.size();
    }

    public void addApp(AppsInfo app) {
        appsList.add(app);
    }

    public void removeApp(int index) {
        appsList.remove(index);
        for (int i = 0; i < getItemCount(); i++) {
            System.out.println("App index " + i);
            System.out.println("App " + appsList.get(i).label);
        }
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        /**
         * 0 3 6
         * 1 4 7
         * 2 5 8
         */

        System.out.println("Move from " + fromPosition + " to " + toPosition);
        for (int i = 0; i < getItemCount(); i++) {
            System.out.println("App index " + i);
            System.out.println("App " + appsList.get(i).label);
        }
        //chrome - contact - setting - play - widget
        //widget - drive - maps - message - photo

        if (Math.abs(fromPosition - toPosition) == 1) {
            System.out.println("Up down");
            Collections.swap(appsList, fromPosition, toPosition);
            this.currentPosition = String.valueOf(toPosition);
        } else {
            if(fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(appsList, i, i + 1);
                    this.currentPosition = String.valueOf(toPosition);
                }
            }
            else{
                System.out.println("here");
                for (int i = fromPosition; i > toPosition; i--) {

                    Collections.swap(appsList, i, i - 1);
                    this.currentPosition = String.valueOf(toPosition);
                }
            }
        }

        for (int i = 0; i < getItemCount(); i++) {

            System.out.println("After qqApp index " + i);
            System.out.println("App " + appsList.get(i).label);

        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, PreferenceManager.OnActivityResultListener, View.OnCreateContextMenuListener {
        public LinearLayout item;
        public TextView appName;
        public ImageView appIcon;
        public TextView appPacakge;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.appsDetail);
            appName = itemView.findViewById(R.id.appsName);
            appIcon = itemView.findViewById(R.id.appsIcon);
            appPacakge = itemView.findViewById(R.id.appsPackage);

            itemView.setOnClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, final View v, ContextMenu.ContextMenuInfo menuInfo) {
            currentPosition = String.valueOf(getAdapterPosition());
            MenuItem delete = menu.add(0, v.getId(), 0, "Delete");//groupId, itemId, order, title
            delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    String app_pkg_name = (String) appPacakge.getText();
                    Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
                    intent.setData(Uri.parse("package:" + app_pkg_name));
                    intent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
                    int UNINSTALL_REQUEST_CODE = 1;
                    ((Activity) v.getContext()).startActivityForResult(intent, UNINSTALL_REQUEST_CODE);

                    System.out.println("new Position------- " + currentPosition);

                    removeApp(Integer.valueOf(currentPosition));
                    notifyItemRemoved(Integer.parseInt(String.valueOf(currentPosition)));
                    return false;
                }
            });

        }


        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            Context context = v.getContext();

            Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(appsList.get(pos).packageName.toString());
            context.startActivity(launchIntent);
        }


        @Override
        public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
            System.out.println("HIIIIIIIIIi");
            return false;
        }
    }


}
