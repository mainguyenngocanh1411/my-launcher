package com.na.mylauncher;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.na.mylauncher.adapters.ItemTouchCallback;
import com.na.mylauncher.adapters.RAdapter;
import com.na.mylauncher.models.AppsInfo;

import java.util.List;
import java.util.Timer;

public class AppsDrawer extends AppCompatActivity {
    public RAdapter radapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apps_drawer);
        //Set view
        RecyclerView recyclerView = findViewById(R.id.appsList);

        radapter = new RAdapter();
        recyclerView.setAdapter(radapter);

        RecyclerView.LayoutManager layoutManager = new
                GridLayoutManager(getApplicationContext(), 4, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        ItemTouchHelper.Callback callback =
                new ItemTouchCallback(radapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);

        touchHelper.attachToRecyclerView(recyclerView);
        new updateApps().execute();
    }


    public class updateApps extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... Params) {
            PackageManager pm = getPackageManager();

            Intent i = new Intent(Intent.ACTION_MAIN, null);
            i.addCategory(Intent.CATEGORY_LAUNCHER);

            //Get list all active apps in device
            List<ResolveInfo> allApps = pm.queryIntentActivities(i, 0);
            for (ResolveInfo ri : allApps) {
                AppsInfo app = new AppsInfo();
                app.label = ri.loadLabel(pm);
                app.packageName = ri.activityInfo.packageName;
                app.icon = ri.activityInfo.loadIcon(pm);
                radapter.addApp(app);
            }

            return "Success";

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            updateStuff();

        }
        public void updateStuff() {
            radapter.notifyItemInserted(radapter.getItemCount()-1);
            radapter.notifyItemRemoved(radapter.getItemCount()-1);
        }

    }


}
